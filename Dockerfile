FROM openjdk:8
ADD target/new-task-0.0.1-SNAPSHOT.jar new-task-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "new-task-0.0.1-SNAPSHOT.jar"]
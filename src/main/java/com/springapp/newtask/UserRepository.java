package com.springapp.newtask;

import org.springframework.data.repository.CrudRepository;

import com.springapp.newtask.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}
